class Position {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  copy() {
    return new Position(this.x, this.y);
  }
}

function newRandomPosition(gameSize) {
  const x = Math.floor(Math.random() * gameSize.width);
  const y = Math.floor(Math.random() * gameSize.height);

  return new Position(x,y);
}

class Size {
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }
}

class DrawWrapper {
  constructor(ctx, cellSize, gameSize) {
    this.ctx = ctx;
    this.cellSize = cellSize;
    this.gameSize = gameSize


    this.borderWidth = 1;
  };

  drawEntity(position, color) {
    const oldFill = this.ctx.fillStyle;
    this.ctx.fillStyle = color;
    this.ctx.fillRect(position.x * this.cellSize.width , position.y * cellSize.height, this.cellSize.width - this.borderWidth, this.cellSize.height - this.borderWidth);
    this.ctx.fillStyle = oldFill;
  }

  drawBackground(color) {
    const oldFill = this.ctx.fillStyle;
    this.ctx.fillStyle = color;
    this.ctx.fillRect(0, 0, this.cellSize.width * this.gameSize.width, this.cellSize.height * this.gameSize.height);
    this.ctx.fillStyle = oldFill;
  }
}

const Direction = Object.freeze({
  UP: 0b0001,
  RIGHT:0b0010,
  DOWN: 0b0100,
  LEFT: 0b1000
});
