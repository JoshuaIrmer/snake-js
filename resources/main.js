const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const rows = 20;
const cols = 20;

let snake = [{
  x: 19,
  y: 3
}];

let food

const cellWidth = canvas.width / cols;
const cellHeight = canvas.height / rows;

let direction = 'LEFT';

let foodCollected = false;

placeFood();

setInterval(gameLoop, 300);

document.addEventListener('keydown', keydown);

draw();

function draw() {
  ctx.fillStyle = 'green';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'black';

  snake.forEach((part) => {
    add(part.x, part.y);
  });

  ctx.fillStyle = 'orange';
  add(food.x, food.y);

  requestAnimationFrame(draw);
}

function reset() {
  placeFood()
  snake = [{
    x: 19,
    y: 3
  }];
  direction = 'LEFT';
}

function testGameOver() {
  const firstPart = snake[0];
  const otherParts = snake.slice(1);
  const duplicatePart = otherParts.find((part) => part.x == firstPart.x && part.y == firstPart.y);

  if (duplicatePart) {
    // parts and body overlap. Bit my tail!
    reset();
    return;
  }

  if (snake[0].x < 0 || snake[0].x > cols - 1 || snake[0].y < 0 || snake[0].y > rows - 1) {
    // snake hit wall. Oh my head!
    reset()
    return;
  }
}

function placeFood() {
  const randomX = Math.floor(Math.random() * cols);
  const randomY = Math.floor(Math.random() * rows);

  food = {
    x: randomX,
    y: randomY
  }
}

function add(x, y) {
  ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth - 1, cellHeight - 1);
}

function shiftSnake() {
  for (let i = snake.length - 1; i > 0; i--) {
    const part = snake[i];
    const lastPart = snake[i - 1];
    part.x = lastPart.x;
    part.y = lastPart.y;
  }
}

function gameLoop() {
  testGameOver();

  if (foodCollected) {
    snake = [
      {
        x: snake[0].x,
        y: snake[0].y
      },
      ...snake
    ];

    foodCollected = false;
  }

  shiftSnake()

  switch (direction) {
    case 'LEFT':
      snake[0].x--;
      break;
    case 'RIGHT':
      snake[0].x++;
      break;
    case 'UP':
      snake[0].y--;
      break;
    case 'DOWN':
      snake[0].y++;
      break;

  }

  if (snake[0].x == food.x && snake[0].y == food.y) {
    foodCollected = true;
    placeFood();
  }
}

function keydown(e) {
  switch (e.keyCode) {
    case 37:
      direction = 'LEFT';
      break;
    case 38:
      direction = 'UP';
      break;
    case 39:
      direction = 'RIGHT';
      break;
    case 40:
      direction = 'DOWN';
      break;
    default:
      console.warn(`unkown key: ${e.key}`);
  }
}
