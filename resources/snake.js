class Snake {
  constructor(position, drawer, direction) {
    this.position = position;
    this.drawer = drawer;
    this.direction = direction;

    this.parts = [];

    this.headColor = '#00BFFF';
    this.bodyColor = '#888888';
  }

  setDirection(direction) {
    if (Object.values(Direction).indexOf(direction) === -1) {
      throw new Error('unkown direction');
    }

    this.direction = direction;
  }

  move() {
    const lastHead = this.position.copy();

    switch (this.direction) {
      case Direction.UP:
        this.position.y--;
        break;
      case Direction.DOWN:
        this.position.y++;
        break;
      case Direction.LEFT:
        this.position.x--;
        break;
      case Direction.RIGHT:
        this.position.x++;
        break;
    }

    if (this.parts.length > 0) {
      this.parts.pop();
      this.parts.unshift(lastHead);
    }
  }

  draw() {
    this.drawer.drawEntity(this.position, this.headColor);
    this.parts.forEach((part) => {
      this.drawer.drawEntity(part, this.bodyColor);
    });
  }
}
